import winsound
import configparser
import os
import glob
from os import listdir
from os.path import isfile, join
import time
import numpy as np
import pandas as pd
global lastLines

class TrackingObject:
    def __init__(self,filename):
        self.location=filename
        self.lastline=None
        self.lastupdate=None
    def processLastLine(self,lastline):
        if lastline==None:
            self.lastline=lastline
            self.lastupdate=time.time()

        else:
            if self.lastline==lastline:

                pass
            else:

                self.lastline=lastline
                self.lastupdate=time.time()

def alert():
    winsound.Beep(293, 200) # D
    winsound.Beep(293, 200) # D
    winsound.Beep(293, 200) # D
    winsound.Beep(293, 600) # D
    winsound.Beep(246, 600) # B

    time.sleep(0.1)

    winsound.Beep(369, 200)# F#
    winsound.Beep(369, 200)# F#
    winsound.Beep(369, 200)# F#
    winsound.Beep(369, 600)# F#
    winsound.Beep(329, 600) # E

    time.sleep(0.1)

    winsound.Beep(329, 200) # E
    winsound.Beep(329, 200) # E
    winsound.Beep(329, 200) # E
    winsound.Beep(369, 500) # F#

    time.sleep(0.9)

    winsound.Beep(369, 200) # F#
    winsound.Beep(369, 200) # F#
    winsound.Beep(369, 200) # F#
    winsound.Beep(369, 600) # F#

    time.sleep(0.9)
    winsound.Beep(369, 200) # F#
    winsound.Beep(369, 200) # F#
    winsound.Beep(369, 200) # F#

    for i in range(4):
        winsound.Beep(369, 200) # F#
        time.sleep(0.1)

    for i in range(4):
        winsound.Beep(369, 100) # F#
        time.sleep(0.1)

    winsound.Beep(369, 600) # F#


def annoy(): 
    alert()
    #playsound('audio.mp3')

def checkFileExists(location):
    return (os.path.isdir(location))

    # Keep preset values

def getFolders(conf):
    folderList=[]
    tracking={}
    for each_section in conf.sections():
        if each_section=="FILE_LOCATIONS":
            for (each_key, each_val) in conf.items(each_section):
                folder=each_val.split("*")[0]
                track=int(each_val.split("*")[-1])
                tracking[folder]=track
                if checkFileExists (folder):

                    folderList.append(folder)
                else:
                    print(folder + " is not a valid folder. Check the config file.")
                    folderList=[]
                    break
    return folderList,tracking


def initConfig(configLocation):
    Config = configparser.ConfigParser()
    Config.read(configLocation)
    return Config 

def getAllFiles(folder):
    return  [f for f in listdir(folder) if isfile(join(folder, f))]

def getNlastLines(N,fileName):
    with open(fileName, 'r') as f:
        lines = f.read().splitlines()
        last_line = lines[-N:]
        return last_line
        

def findLastModified(folder,startTime):
    return os.path.getmtime(folder)


def lastUpdated(folders,startTime):
    last_updates={}
    for folder in folders:
        allfiles= getAllFiles(folder)
        for file in allfiles:
            last_updates[folder+"/"+file]=findLastModified(folder+"/"+file,startTime)
    return last_updates


def getThold(conf):
    return int((conf["PARAMETERS"]["triggerSeconds"]))

def getFreq(conf):
    return int((conf["PARAMETERS"]["warnFreqSeconds"]))



def filterLastUpdated(lastUpdatedFiles,tracking,startTime):

    folders=list((["\\".join(x.split("\\")[:-1]) for x in lastUpdatedFiles.keys()]))
    files=[x for x in lastUpdatedFiles.keys()]
    times=[lastUpdatedFiles[x] for x in lastUpdatedFiles.keys()]
    globList=[]
    for ids in range(len(times)):
        globList.append([folders[ids],files[ids],times[ids]])
    df=pd.DataFrame(globList,columns=["Folder","File","Time"])
    filesToTrack=[]
    for folder in df["Folder"].unique():
        dff=df[df["Folder"]==folder].sort_values(["Time"],ascending=False)
        dff=dff[dff["Time"]>startTime]
        dff=dff.iloc[:tracking[folder+"\\"],:]
        mylist=(list(dff.File))
        for x in mylist:
            filesToTrack.append(x)
    return (filesToTrack)

def processFiles(tracingObjects,filestoTrack):

    #if there is no object add it, and process last line
    #if there is object process last line
    for x in filestoTrack:
        if x not in [x.location for x in tracingObjects]:

            obj = TrackingObject(x)
            obj.processLastLine(getNlastLines(1,x)[0])
            tracingObjects.append(obj)
    for obj in tracingObjects:
        obj.processLastLine(getNlastLines(1,obj.location)[0])
    return tracingObjects


def checktimes(tracingObjects,conf):

    a=0
    if len(tracingObjects)==0:
        print("Nothing to Track Yet")
    for obj in tracingObjects:
        print("Tracing: "+str(obj.location)+" Last Updated: "+str(np.round(obj.lastupdate,0)) + " Delay: " + str(np.round(time.time()-obj.lastupdate,0)))
        if time.time()-obj.lastupdate>getThold(conf):
            print("******Delayed Log: " +obj.location)
            a+=1
    if a ==0 :
        return 0
    else:
        return 1
def removexpiredObjects(tracingObjects,filestoTrack):
    response=[]
    for obj in tracingObjects:
        if obj.location  in [x for x in filestoTrack]: 
            response.append(obj)
    return response
    




def main(configFile,lastAnnoyed,startTime,lastLines,tracingObjects):
    
    conf = initConfig(configFile)
    folders,tracking= getFolders(conf)
    
    if folders==[]:
        raise Exception('Check Config File!')
    else:
         lastUpdatedFiles=lastUpdated(folders,startTime)
         filestoTrack=filterLastUpdated(lastUpdatedFiles,tracking,startTime)
         
         tracingObjects=processFiles(tracingObjects,filestoTrack)
         tracingObjects=removexpiredObjects(tracingObjects,filestoTrack)
         alert=checktimes(tracingObjects,conf)
        
         if alert==1 and  time.time()-lastAnnoyed>getFreq(conf):
            print("Warning Triggered!")
            annoy()
            lastAnnoyed=time.time()
    return tracingObjects,lastAnnoyed
        

if __name__ == '__main__':
    lastAnnoyed=0
    startTime = time.time()
    configFile= "config.ini"
    lastLines={}
    tracingObjects=[]
    while True: 
        print("-"*100)
        tracingObjects,lastAnnoyed=main(configFile,lastAnnoyed,startTime,lastLines,tracingObjects)
        time.sleep(5)

