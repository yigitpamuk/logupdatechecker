
## LogUpdateChecker
This tool plays an alert sound if specified log folders are not updated within the given time interval.

Dependencies
1. Numpy
2. Winsound
3. configparser
4. glob


---

## Instructions

1. Add folders  to track in config.py
2. Set alert frequency and triggerin config.py
3. python LogAlerter.py

